const httpHeaders = {
  "Accept": "application/json, text/plain, */*",
  "Accept-Encoding": "gzip, deflate, br",
  "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8",
  "Cache-Control": "no-cache",
  "Connection": "keep-alive",
  "Content-Type": "application/json",
  "Cookie":
      "_ga=GA1.2.1684435026.1542631291; Hm_lvt_022f847c4e3acd44d4a2481d9187f1e6=1549879110,1551271481; _gid=GA1.2.304898857.1551271481; GCID=0aa214e-77cd772-a4a994c-c1a1c45-cd; _gat=1; SERVERID=1fa1f330efedec1559b3abbcb6e30f50|1551271951|1551271478; Hm_lpvt_022f847c4e3acd44d4a2481d9187f1e6=1551271955",
  "Host": "time.geekbang.org",
  "Origin": "https://time.geekbang.org",
  "Pragma": "no-cache",
  "Referer": "https://time.geekbang.org/",
  "User-Agent":
      "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36"
};
