import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:io';
import '../config/service_url.dart';

// 获取首页主题内容
Future request(url, formData) async {
  try {
    print('开始获取数据');
    Response response;
    Dio dio = new Dio();
    dio.options.contentType = ContentType.parse("application/x-www-form-urlencoded");
    if (formData == null) {
      response = await dio.post(servicePath[url]);
    } else {
      response = await dio.post(servicePath[url], data: formData);
    }
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('后端接口出现异常。');
    }
  } catch (e) {
    print('ERROR： ===========> ${e}');
  }
}

// 获取首页主题内容
Future getHomePageContent() async {
  var formData = {'lon': '115.02932', 'lat': '35.76189'};
  return request('homePageContent', formData);
}

// 获取火爆专区的商品
Future getHomePageBelowConten(formData) async {
  return request('homePageBelowConten', formData);
}


// 获取分类
Future getCategory() async {
  return request('getCategory', null);
}


// 获取分类商品列表
Future getMallGoods(formData) async {
  return request('getMallGoods', formData);
}
